# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  str.chars.each do |letter|
    str.delete!(letter) if letter == letter.downcase
  end

  str
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  mid_point = str.length/2
  if (str.length % 2) == 0
    middle_str = str[mid_point-1] + str[mid_point]
  else
    middle_str = str[mid_point]
  end
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  vowel_counter = 0

  str.chars.each do |letter|
    vowel_counter += 1 if VOWELS.include?(letter.downcase)
  end

  vowel_counter

end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  accum = 1
  (1..num).each do |number|
    accum *= number
  end
  accum
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  joined = ""
  arr.each_with_index do |ele, idx|
    if idx == (arr.length - 1)
      joined += ele
    else
      joined = joined + ele + separator
    end
  end
  joined
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  weird_case = str.chars

  weird_case.each_with_index do |letter, idx|
    if idx.even?
      weird_case[idx] = letter.downcase
    else
      weird_case[idx] = letter.upcase
    end
  end
  weird_case.join
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  new_sentence = []

  str.split.each do |word|
    word = word.reverse if word.length >= 5
    new_sentence << word
    end

  new_sentence.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  arr = (1..n).to_a

  arr.each_with_index do |num, idx|
    if num % 15 == 0
      arr[idx] = "fizzbuzz"
    elsif num % 5 == 0
      arr[idx] = "buzz"
    elsif num % 3 == 0
      arr[idx] = "fizz"
    end
  end

  arr
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)

end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)

end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)

end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)

end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)

end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)

end

# Target completion date: April 30, 2017
